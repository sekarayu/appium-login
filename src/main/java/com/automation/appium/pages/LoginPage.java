package com.automation.appium.pages;

/**
 * Created by sekar on 25/03/2018.
 */
public class LoginPage {

    public static final String loginPageTitle = "//android.widget.TextView[@text='Athena Login Sample']";
    public static final String username = "com.naspers_classifieds.loginsample:id/username";
    public static final String password = "com.naspers_classifieds.loginsample:id/password";
    public static final String loginBtn = "com.naspers_classifieds.loginsample:id/login";
    public static final String loginMsg = "com.naspers_classifieds.loginsample:id/message";
    public static final String loginSuccess = "Success!";
    public static final String loginFailed = "Failure!";

}
