package com.automation.appium.steps;

import com.automation.appium.driver.AppiumHelper;
import com.automation.appium.pages.LoginPage;
import org.junit.Assert;

/**
 * Created by sekar on 25/03/2018.
 */
public class LoginSteps extends AppiumHelper{

    private static void verifyLoginPageTitle(){
        Assert.assertTrue(isElementPresent(getXpathLocator(LoginPage.loginPageTitle)));
    }

    private static void verifyUsername(){
        Assert.assertTrue(isElementPresent(getIdLocator(LoginPage.username)));
    }

    private static void verifyPassword(){
        Assert.assertTrue(isElementPresent(getIdLocator(LoginPage.password)));
    }

    private static void verifyLoginBtn(){
        Assert.assertTrue(isElementPresent(getIdLocator(LoginPage.loginBtn)));
    }

    public static void verifyLoginPage(){
        verifyLoginPageTitle();
        verifyUsername();
        verifyPassword();
        verifyLoginBtn();
    }

    public static void theUserIsOnLoginPage(){
        verifyLoginPageTitle();
    }

    public static void fillUsername(String user){
        sendKeys(getIdLocator(LoginPage.username), user);
        hideKeyboard();
    }

    public static void fillPassword(String password){
        sendKeys(getIdLocator(LoginPage.password), password);
        hideKeyboard();
    }

    public static void clickLoginBtn(){
        clickElement(getIdLocator(LoginPage.loginBtn));
    }

    public static void checkLoginSuccess(){
        Assert.assertTrue("Login message not match!",
                getTextFromElement(getIdLocator(LoginPage.loginMsg)).equalsIgnoreCase(LoginPage.loginSuccess));
    }

    public static void checkLoginFailed(){
        Assert.assertTrue("Login message not match!",
                getTextFromElement(getIdLocator(LoginPage.loginMsg)).equalsIgnoreCase(LoginPage.loginFailed));
    }
}
