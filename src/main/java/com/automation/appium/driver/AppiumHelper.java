package com.automation.appium.driver;

import cucumber.api.Scenario;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.DefaultExecuteResultHandler;
import org.apache.commons.exec.DefaultExecutor;
import org.apache.commons.exec.ExecuteException;
import org.openqa.selenium.*;
import org.openqa.selenium.net.UrlChecker;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

/**
 * Created by sekar on 25/03/2018.
 */
public class AppiumHelper {

    final static Logger LOGGER = LoggerFactory.getLogger(AppiumHelper.class);
    public static Scenario CURRENT_SCENARIO;
    public static boolean EXECUTE_TEST = true;
    private static AndroidDriver<AndroidElement> driver;
    private static File app;
    private static DesiredCapabilities CAPABILITIES;
    public static AndroidDriver<AndroidElement> getDriver() { return driver; }
    private static boolean INITIALIZED = false;
    public static Properties CONFIG_PROPERTIES=null;
    private static final String APPIUM_URL = "appium.url";
    private static final String APPIUM_PORT = "appium.port";
    private static final String APPIUM_LOCATION = "appium.location";

    private static void setCapabilities()
    {
        try
        {
            CAPABILITIES = new DesiredCapabilities();
            CAPABILITIES.setCapability("platformName", "Android");
            CAPABILITIES.setCapability("fullReset", "true");
            CAPABILITIES.setCapability("app", Constant.app.getAbsolutePath());
            CAPABILITIES.setCapability("appPackage",Constant.appPackage);
            CAPABILITIES.setCapability("appActivity",Constant.appActivity);
            CAPABILITIES.setCapability("deviceName", "Galaxy");
            //CAPABILITIES.setCapability("noReset", true);
            CAPABILITIES.setCapability("newCommandTimeout","120");
            CAPABILITIES.setCapability("recreateChromeDriverSessions",true);

            LOGGER.info("Desired capabilities : "+CAPABILITIES.toString());
        }
        catch(Exception e)
        {
            LOGGER.error("Error occured while setting capabilities:"+e.fillInStackTrace());
        }
    }

    private static Properties readConfigProperties() throws IOException
    {
        String propertiesFileName = "config.properties";
        ClassLoader loader = Thread.currentThread().getContextClassLoader();
        Properties properties = new Properties();
        try(InputStream resourceStream = loader.getResourceAsStream(propertiesFileName)) {
            properties.load(resourceStream);
        }
        return properties;
    }

    private static String getAppiumServerUrl() throws IOException {
        if(CONFIG_PROPERTIES==null)
        {
            CONFIG_PROPERTIES = readConfigProperties();
        }
        System.out.println("Appium Server URL: " + String.format("%s:%s/wd/hub",getAppiumServerAddress(), getAppiumPort()));
        return String.format("%s:%s/wd/hub",getAppiumServerAddress(), getAppiumPort());
    }

    private static String getAppiumServerAddress() throws IOException {
        if(CONFIG_PROPERTIES==null)
        {
            CONFIG_PROPERTIES = readConfigProperties();
        }


        return (String) CONFIG_PROPERTIES.get(APPIUM_URL);

    }

    private static String getAppiumPort() throws IOException {
        if(CONFIG_PROPERTIES==null)
        {
            CONFIG_PROPERTIES = readConfigProperties();
        }
        String port = (String) CONFIG_PROPERTIES.get(APPIUM_PORT);
        return port;
    }

    private static String getAppiumLocation() throws IOException {
        if(CONFIG_PROPERTIES==null)
        {
            CONFIG_PROPERTIES = readConfigProperties();
        }
        String location = (String) CONFIG_PROPERTIES.get(APPIUM_LOCATION);
        return location;
    }

    public static void loadDriver()
    {
        try
        {
            setCapabilities();
            driver = new AndroidDriver<AndroidElement>(new URL(getAppiumServerUrl()), CAPABILITIES);
        }
        catch(Exception e)
        {
            LOGGER.error("Unable to create instance of android driver:"+e.fillInStackTrace());
            throw new RuntimeException("Unable to create instance of android driver:"+e.fillInStackTrace());
        }
    }

    public static void saveScreenShot(String fileNameSufix, Scenario scenario)
    {
        if (getDriver() instanceof TakesScreenshot)
        {
            TakesScreenshot camera = (TakesScreenshot) getDriver();
            byte[] screenshot = camera.getScreenshotAs(OutputType.BYTES);
            scenario.embed(screenshot, "image/png");
        }
    }
    public static void closeDriver()
    {
        try
        {
            if(driver==null)
            {
                LOGGER.info("Driver is already closed");
            }
            else
            {
                try
                {
                    saveScreenShot("", CURRENT_SCENARIO);
                }
                finally
                {
                    driver.quit();
                    LOGGER.info("Driver closed successfully.");
                    driver=null;
                }
            }
        }
        catch(Exception e)
        {
            LOGGER.error("Error occured while closing driver :"+e.getStackTrace());
        }
    }

    public static boolean startAppiumServer() throws ExecuteException, IOException {
        DefaultExecuteResultHandler resultHandler = new DefaultExecuteResultHandler();
        String comm = getAppiumLocation()+ " -a "+ getAppiumServerAddress().replace("http://", "")
                +" -p " +getAppiumPort() + " --session-override";
        LOGGER.info("Executing command:"+comm);
        try {
            CommandLine command = CommandLine.parse(comm);
            DefaultExecutor executor = new DefaultExecutor();
            executor.execute(command,resultHandler);
            LOGGER.info("Command executed successfully:"+comm);
        } catch(Exception e) {
            LOGGER.error("Unable to execute appium start command!");
            return false;
        }
        try {
            URL status = new URL(getAppiumServerUrl() + "/sessions");
            new UrlChecker().waitUntilAvailable(50, TimeUnit.SECONDS, status);
            LOGGER.info("Appium server started successfully at :"+getAppiumServerUrl());
            return true;
        } catch (Exception e) {
            LOGGER.error("Appium server not started within expected time!");
        }
        return false;
    }

    public static void init() {
        if(!INITIALIZED) {
            try {
                INITIALIZED = true;
                if(AppiumHelper.startAppiumServer()){
                    AppiumHelper.loadDriver();
                    LOGGER.info("Driver initialized successfully.");
                }else {
                    //stop execution for remaining test cases
                    AppiumHelper.EXECUTE_TEST=false;
                }
            } catch (Exception e) {
                LOGGER.error("Error while initializing driver:"+e.fillInStackTrace());
                LOGGER.error(String.valueOf(e.fillInStackTrace()));
                AppiumHelper.EXECUTE_TEST=false;
            }
        }
    }

    public static By getIdLocator(String locator){
        return By.id(locator);
    }

    public static By getXpathLocator(String locator){
        return By.xpath(locator);
    }

    public static boolean isElementPresent(By locator) {
        try {
            if (driver.findElement(locator).isDisplayed()){
                return true;
            }else{
                return false;
            }
        } catch (NoSuchElementException | TimeoutException e) {
            LOGGER.info("Element "+locator+" not found");
            return false;
        }
    }

    public static void sendKeys(By locator, String value){
        try {
            WebElement element = driver.findElement(locator);
            element.sendKeys(value);
        }catch (Exception e){
            LOGGER.info("Unable to perform send keys on element "+locator+"! Error :"+e.fillInStackTrace());
        }
    }

    public static void clickElement(By locator){
        try {
            WebElement element = driver.findElement(locator);
            element.click();
        }catch (Exception e){
            LOGGER.info("Unable to perform click on element "+locator+"! Error :"+e.fillInStackTrace());
        }
    }

    public static String getTextFromElement(By locator){
        String text = "";
        try {
            WebElement element = driver.findElement(locator);
            text = element.getText();
        }catch (Exception e){
            LOGGER.info("Unable to get text from element "+locator+"! Error :"+e.fillInStackTrace());
        }
        return text;
    }

    public static void hideKeyboard(){
        try {
            getDriver().hideKeyboard();
        }catch (Exception e){
            LOGGER.info(e.getMessage());
        }

    }
}
