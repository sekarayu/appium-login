package com.automation.appium.driver;

import java.io.File;

/**
 * Created by sekar on 25/03/2018.
 */
public class Constant {
    public static File app = new File(System.getProperty("user.dir") + "/" + System.getProperty("apkPath"));
    public static final String appPackage = "com.naspers_classifieds.loginsample";
    public static final String appActivity = "com.naspers_classifieds.loginsample.MainActivity";
}
