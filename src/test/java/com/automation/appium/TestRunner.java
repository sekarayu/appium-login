package com.automation.appium;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

/**
 * Created by sekar on 25/03/2018.
 */
@RunWith(Cucumber.class)
@CucumberOptions(
        strict = false,
        features="src/test/resources/features",
        //glue={"classpath:com/automation/appium/stepDefinitions/"},
        format = {"junit:target/reports/junit.xml", "pretty","html:target/reports/cucumber-reports","json:target/cucumber.json" }
        ,tags = { "~@ignore, @login" }
)
public class TestRunner {
}
