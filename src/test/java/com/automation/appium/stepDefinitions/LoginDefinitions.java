package com.automation.appium.stepDefinitions;

import com.automation.appium.steps.LoginSteps;
import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

/**
 * Created by sekar on 25/03/2018.
 */
public class LoginDefinitions {

    @Given("^the user opens app$")
    public void the_user_opens_app() throws Throwable
    {
        Hook.init();
    }

    @Then("^the user should see login page$")
    public void theUserShouldSeeLoginPage() throws Throwable {
        LoginSteps.verifyLoginPage();
    }

    @Given("^the user is on login page$")
    public void theUserIsOnLoginPage() throws Throwable {
        LoginSteps.theUserIsOnLoginPage();
    }

    @And("^the user clicks on login button$")
    public void theUserClicksOnLoginButton() throws Throwable {
        LoginSteps.clickLoginBtn();
    }

    @Then("^the user will logged in$")
    public void theUserWillLoggedIn() throws Throwable {
        LoginSteps.checkLoginSuccess();
    }

    @When("^the user fill username \"([^\"]*)\"$")
    public void theUserFillUsername(String arg0) throws Throwable {
        LoginSteps.fillUsername(arg0);
    }

    @And("^the user fill password \"([^\"]*)\"$")
    public void theUserFillPassword(String arg0) throws Throwable {
        LoginSteps.fillPassword(arg0);
    }

    @Then("^the user will see login fail message$")
    public void theUserWillSeeLoginFailMessage() throws Throwable {
        LoginSteps.checkLoginFailed();
    }
}
