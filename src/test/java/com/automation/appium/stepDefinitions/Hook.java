package com.automation.appium.stepDefinitions;

import com.automation.appium.driver.AppiumHelper;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import org.junit.Assume;
import org.junit.AssumptionViolatedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by sekar on 25/03/2018.
 */
public class Hook {
    final static Logger LOGGER = LoggerFactory.getLogger(Hook.class);

    @Before
    public void beforeScenario(Scenario scenario) throws AssumptionViolatedException
    {
        LOGGER.info("********* Automation execution started for scenario:"+scenario.getName());
        AppiumHelper.CURRENT_SCENARIO=scenario;
        try
        {
            Assume.assumeTrue(AppiumHelper.EXECUTE_TEST);
        }
        catch(Exception e)
        {
            AppiumHelper.closeDriver();
            throw new AssumptionViolatedException("Rest of the test cases will not be executed due to assumption violated:"+e.fillInStackTrace());
        }
    }
    @After
    public void afterScenario(Scenario scenario) throws Exception
    {
        LOGGER.info("********* Automation execution Completed for scenario:"+scenario.getName());
        if(scenario.isFailed())
        {
            AppiumHelper.saveScreenShot(scenario.getName().replace(' ', '_'), scenario);
        }
    }

    @Before("@init")
    public static void init() {
        AppiumHelper.init();
    }
}
