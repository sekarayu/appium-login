@login
  Feature: Login

    Scenario: Verify login page
      Given the user opens app
      Then the user should see login page

    Scenario: Login success
      Given the user is on login page
      When the user fill username "athena@olx.com"
      And the user fill password "123456"
      And the user clicks on login button
      Then the user will logged in

    Scenario: Login failed - Invalid Username
      Given the user is on login page
      When the user fill username ""
      And the user fill password "123456"
      And the user clicks on login button
      Then the user will see login fail message

    Scenario: Login failed - Invalid Password
      Given the user is on login page
      When the user fill username "athena@olx.com"
      And the user fill password ""
      And the user clicks on login button
      Then the user will see login fail message