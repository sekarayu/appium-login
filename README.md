# README #

# Tools :
- Appium
- Maven
- Java 1.8
- Cucumber

# How to run the test :
- open emulator/connect an Android device
- command : mvn clean install -DapkPath=athena-login-sample.apk
- it will start appium server and run the test

# Appium config :
- Location : src/main/resources/config.properties
- Change the configuration based on your local appium setting
- appium.location : path to the installed appium
- appium.url : url of the appium
- appium.port : port of the appium

# Report :
- Report is located inside : target/cucumber-html-reports/cucumber-html-reports/overview-features.html